package com.list.fans.mapper;

import com.list.fans.model.Transpond;
import org.springframework.stereotype.Repository;

@Repository
public interface TranspondMapper {
    /**
     * 插入转发
     * @param transpond
     */
    void insertTrans(Transpond transpond);

    /**
     * 删除转发
     * @param transpondId
     */
    void deleteTrans(String transpondId);

    /**
     * 点赞
     * @param transpondId
     */
    void updateTransPraiseUp(String transpondId);

    /**
     * 取消点赞
     * @param transpondId
     */
    void updateTransPraiseDown(String transpondId);

    /**
     * 转发数+1
     */
    void updateTransCountUp(String transpondId);

    /**
     * 转发数+1
     */
    void deleteTransCountDown(String transpondId);
}
