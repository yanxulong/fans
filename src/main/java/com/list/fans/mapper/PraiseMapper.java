package com.list.fans.mapper;


import com.list.fans.model.Praise;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PraiseMapper {

    /**
     * 点赞
     * @param praise
     */
    void insertPraiseByPost(Praise praise);

    /**
     * 取消点赞
     * @param praise
     */
    void deletePraiseByPost(Praise praise);
    /**
     * 根据用户id查询用户点赞过的帖子
     * @param userid
     * @return
     */
    public List<Praise> selectpraisepost(@Param("userid")String userid);
}
