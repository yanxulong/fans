package com.list.fans.mapper;

import com.list.fans.model.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentMapper {
    /**
     * 插入评论
     * @param comment
     */
    void insertComment(Comment comment);

    /**
     * 删除
     * @param commentId
     */
    void deleteComment(String commentId);

    /**
     * 根据用户id查询用户的所有评论
     * @param commentUserId
     * @return
     */
    public List<Comment> selectComment(@Param("commentUserId")String commentUserId);

    /**
     * 根据帖子id查询本帖子的所有评论
     * @param postid
     * @return
     */
    public List<Comment> selectcommentbypostid(@Param("postid") String postid);
}
