package com.list.fans.mapper;

import com.list.fans.model.Collect;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectMapper {
    /**
     * 加入收藏
     */
    void insertCollect(Collect collect);

    /**
     * 删除收藏
     */
    void deleteCollect(String collectId);
    /**
     *根据用户id查询用户收藏的帖子
     * @param userid
     * @return
     */
    public List<Collect> selectcollectpost(@Param("userid") String userid);
}
