package com.list.fans.aspectj;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogAspectj {
    private static Logger logger = LoggerFactory.getLogger(LogAspectj.class);

    /**
     * 之前打印日志
     */
    public void before(){
        logger.info("打印前");
    }
    /**
     * 之后打印日志
     */
    public  void after(){
        logger.info("打印后");
    }
}
