package com.list.fans.model;
import java.util.List;

/**
 * 点赞  “赞”的实体类
 */
public class Praise {

    private String praiseId;
    private String praisePostId;
    private String praiseCommentId;
    private String praiseUserId;
    private String praiseDateTime;
    private String praiseStatus;

    public String getPraiseId() {
        return praiseId;
    }

    public void setPraiseId(String praiseId) {
        this.praiseId = praiseId;
    }

    public String getPraisePostId() {
        return praisePostId;
    }

    public void setPraisePostId(String praisePostId) {
        this.praisePostId = praisePostId;
    }

    public String getPraiseCommentId() {
        return praiseCommentId;
    }

    public void setPraiseCommentId(String praiseCommentId) {
        this.praiseCommentId = praiseCommentId;
    }

    public String getPraiseUserId() {
        return praiseUserId;
    }

    public void setPraiseUserId(String praiseUserId) {
        this.praiseUserId = praiseUserId;
    }

    public String getPraiseDateTime() {
        return praiseDateTime;
    }

    public void setPraiseDateTime(String praiseDateTime) {
        this.praiseDateTime = praiseDateTime;
    }

    public String getPraiseStatus() {
        return praiseStatus;
    }

    public void setPraiseStatus(String praiseStatus) {
        this.praiseStatus = praiseStatus;
    }


    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }

    List<Post> post;

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    List<Photo> photo;

}
