package com.list.fans.model;

public class Category {

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDateTime() {
        return categoryDateTime;
    }

    public void setCategoryDateTime(String categoryDateTime) {
        this.categoryDateTime = categoryDateTime;
    }

    public String getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(String categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    private String categoryId;
    private String categoryName;
    private String categoryDateTime;
    private String categoryStatus;
}
