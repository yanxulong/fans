package com.list.fans.model;

import java.util.List;

public class Collect {

    public String getCollectId() {
        return collectId;
    }

    public void setCollectId(String collectId) {
        this.collectId = collectId;
    }

    public String getCollectUserId() {
        return collectUserId;
    }

    public void setCollectUserId(String collectUserId) {
        this.collectUserId = collectUserId;
    }

    public String getCollectPostId() {
        return collectPostId;
    }

    public void setCollectPostId(String collectPostId) {
        this.collectPostId = collectPostId;
    }

    public String getCollectDateTime() {
        return collectDateTime;
    }

    public void setCollectDateTime(String collectDateTime) {
        this.collectDateTime = collectDateTime;
    }

    public String getCollectStatus() {
        return collectStatus;
    }

    public void setCollectStatus(String collectStatus) {
        this.collectStatus = collectStatus;
    }

    private String collectId;
    private String collectUserId;
    private String collectPostId;
    private String collectDateTime;
    private String collectStatus;
    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }

    List<Post> post;

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    List<Photo> photo;
}
