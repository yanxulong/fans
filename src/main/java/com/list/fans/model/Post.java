package com.list.fans.model;

import java.util.List;

public class Post {

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }



    public String getPostLikeCount() {
        return postLikeCount;
    }

    public void setPostLikeCount(String postLikeCount) {
        this.postLikeCount = postLikeCount;
    }

    public String getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(String postUserId) {
        this.postUserId = postUserId;
    }

    public String getPostCategoryId() {
        return postCategoryId;
    }

    public void setPostCategoryId(String postCategoryId) {
        this.postCategoryId = postCategoryId;
    }

    public String getPostDateTime() {
        return postDateTime;
    }

    public void setPostDateTime(String postDateTime) {
        this.postDateTime = postDateTime;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }


    public String getPostTranspondCount() {
        return postTranspondCount;
    }

    public void setPostTranspondCount(String postTranspondCount) {
        this.postTranspondCount = postTranspondCount;
    }

    private String postId;
    private String postContent;
    private String postTranspondCount;
    private String postLikeCount;
    private String postUserId;
    private String postCategoryId;
    private String postDateTime;
    private String postStatus;

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    private List<Photo> photo;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private User user;
}
