package com.list.fans.controller;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.list.fans.common.Config;
import com.list.fans.common.Constant;
import com.list.fans.common.HttpUtil;
import com.list.fans.common.JsonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 获取验证码短信验证码接口
 * 传入参数 telphone 手机号
 *
 */


@Controller
public class SMSCheckController{


    private static Logger logger = LoggerFactory.getLogger(SMSCheckController.class);

    @RequestMapping(value ="/api/smscheck",method = RequestMethod.POST)
	@ResponseBody
	protected Object SMSCheck(@RequestParam("userTelephone") String userTelephone){
        JsonResult results;
        try{
            logger.info("参数:{}",userTelephone);
        //生成6位验证码
		int code=(int)(Math.random()*(9999-1000+1))+100000;
		// 将认证码存入SESSION
	   // httpSession.setAttribute("code", code);
		String smsContent = "【唐莎科技】尊敬的用户，您的验证码为"+code+"";
		String to =userTelephone;
		String tmpSmsContent = null;
	        tmpSmsContent = URLEncoder.encode(smsContent, "UTF-8");
	    String url = Config.BASE_URL + Constant.OPERATION;
	    String body = "accountSid=" + Constant.ACCOUNTSID + "&to=" + to + "&smsContent=" + tmpSmsContent + HttpUtil.createCommonParam();
	    String result = HttpUtil.post(url, body);
	    System.out.println("result:" + System.lineSeparator() + result);
            results=new JsonResult("请求成功","200",code);
        }catch(Exception e){
            logger.info(e.getMessage());
            results=new JsonResult("出错了","500");
        }
        return results;
	}
}