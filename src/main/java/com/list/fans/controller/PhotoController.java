package com.list.fans.controller;

import com.list.fans.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Photo;
import com.list.fans.service.PhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.jar.JarEntry;
@Controller
public class PhotoController {
    //程序中声明日志文件
    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    PhotoService photoService;
    /**
     * 根据用户id查询用户所有的相册
     * 接口/select/photo/byuserid
     * 传入参数userid\pageNum\pageSize
     * 请求类型post
     * @param userid
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/photo/byuserid",method = RequestMethod.POST)
    @ResponseBody
    public  Object selectPhoto(
            @RequestParam(value = "userid",defaultValue = "",required = false)String userid,
            @RequestParam(value = "pageNum",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(value = "pageSize")Integer pageSize
    ){
        JsonResult result=null;
        try{
            logger.info("参数:{}{}{}",userid,pageNum,pageSize);
            PageHelper.startPage(pageNum,pageSize);
            List<Photo> list=photoService.selectPhoto(userid);
            PageInfo<Photo> pageInfo=new PageInfo<>(list,pageSize);
            result=new JsonResult("查询成功","200",list);
        }catch (Exception ex){
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return  result;
    }
}
