package com.list.fans.interceptor;

import com.google.gson.Gson;
import com.list.fans.common.JsonResult;
import com.list.fans.model.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthcInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        HttpSession session = httpServletRequest.getSession(false);
        if (session != null) {
            User user = (User) session.getAttribute("loginSuccessKey");
            if (user != null) {
                return true;
            }
            String requestType=httpServletRequest.getHeader("X-Requested-With");
            if("XMLHttpRequest".equals(requestType)){
                JsonResult result=new JsonResult("403","没有登录");
                Gson gson=new Gson();
                 String str=  gson.toJson(result);
                httpServletResponse.getWriter().print(str);
            }else{
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/login2");            }
        }
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/login2");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
