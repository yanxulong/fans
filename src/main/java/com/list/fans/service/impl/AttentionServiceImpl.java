package com.list.fans.service.impl;

import com.list.fans.mapper.AttentionMapper;
import com.list.fans.model.Attention;
import com.list.fans.service.AttentionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttentionServiceImpl implements AttentionService {
    @Autowired
    AttentionMapper attentionMapper;
    /**
     * 查询我的关注：我关注的用户
     * @param userId
     * @return
     */
    public List<Attention> selectAttentionListByUserIdWithUsers(String userId){
        return attentionMapper.selectAttentionListByUserIdWithUsers(userId);
    }

    /**
     * 添加我的关注
     * @param attention
     */
   public void insertAttentionUser (Attention attention){
       attentionMapper.insertAttentionUser(attention);
   }

    /**
     * 删除我的关注
     * @param attentionUserID
     * @param attentionPassiveUserId
     */
   public void deletetAttentionUser(String attentionUserID,String attentionPassiveUserId){
       attentionMapper.deletetAttentionUser(attentionUserID,attentionPassiveUserId);
   }
}
