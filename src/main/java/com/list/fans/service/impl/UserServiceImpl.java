package com.list.fans.service.impl;

import com.list.fans.mapper.UserMapper;
import com.list.fans.model.User;
import com.list.fans.service.UserService;
import com.list.fans.util.UidGenerators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    /**
     *用户登录
     * @param account  账号（手机号/邮箱）
     * @param userPassword  密码
     * @return
     */
    @Override
    public User userLogin(String account, String userPassword) {
        return userMapper.userLogin(account,userPassword);
    }

    /**
     *修改密码
     * @param userId
     * @param userPassword
     * @return
     */
    @Override
    public User updateUserPassword(String userId, String userPassword) {
        //修改密码
        userMapper.updateUserPassword(userId,userPassword);
        //查询修改后的
        return userMapper.selectUserByUserId(userId);
    }

    /**
     * 查询用户名是否重复
     * @param userName
     * @return
     */
    @Override
    public User selectUserNameIsOk(String userName) {
        return userMapper.selectUserNameIsOk(userName);
    }
    /**
     * 查询用户电话是否重复
     * @param userTelephone
     * @return
     */
    @Override
    public User selectUserTelephoneIsOk(String userTelephone) {
        return userMapper.selectUserTelephoneIsOk(userTelephone);
    }
    /**
     * 查询用户邮箱是否重复
     * @param userEmail
     * @return
     */
    @Override
    public User selectUserEmailIsOk(String userEmail) {
        return userMapper.selectUserEmailIsOk(userEmail);
    }

    /**
     * 注册用户
     * @param user
     * @return
     */
    @Override
    public int insertUser(User user) {
        return userMapper.insertUser(user);
    }


    /**
     * 根据用户id查询用户信息@xyb
     * @param userid
     * @return
     */
    @Override
    public User selectUserMessage(String userid) {
        return userMapper.selectUserMessage(userid);
    }

    /**
     * 根据用户id修改用户头像@xyb
     * @param photo
     * @param userid
     */
    @Override
    public void updateUserPhoto(String photo, String userid) {
        userMapper.updateUserPhoto(photo,userid);
    }

    /**
     * 根据用户id修改用户密码
     * @param password
     * @param userid
     */
//    @Override
//    public void updateUserPassword(String password, String userid) {
//        userMapper.updateUserPassword(password,userid);
//    }

    /**
     * 根据用户id修改用户信息
     * @param user
     */
    @Override
    public void updateUserMessage(User user) {
        userMapper.updateUserMessage(user);
    }

    /**
     * 根据用户id查询用户的粉丝数量
     * @param userid
     * @return
     */
    @Override
    public Integer selectfans(String userid) {
        return userMapper.selectfans(userid);
    }

    /**
     * 用户登录
     * @param userTelephone  手机号
     * @return
     */
    @Override
    public User userLoginByCode(String userTelephone) {
        return userMapper.userLoginByCode(userTelephone);
    }
}
