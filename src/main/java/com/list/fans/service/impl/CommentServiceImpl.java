package com.list.fans.service.impl;

import com.list.fans.mapper.CommentMapper;
import com.list.fans.model.Comment;
import com.list.fans.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentMapper commentMapper;

    /**
     * 插入评论
     * @param comment
     */
    public void insertComment(Comment comment){
        commentMapper.insertComment(comment);
    }

    /**
     * 删除评论
     * @param commentId
     */
    public void deleteComment(String commentId){
        commentMapper.deleteComment(commentId);
    }

    /**
     * 根据用户id查询用户发表过的评论
     * @param commentUserId
     * @return
     */
    @Override
    public List<Comment> selectComment(String commentUserId) {
        return commentMapper.selectComment(commentUserId);
    }

    /**
     * 根据帖子id查询评论
     * @param postid
     * @return
     */
    @Override
    public List<Comment> selectcommentbypostid(String postid) {
        return commentMapper.selectcommentbypostid(postid);
    }
}
