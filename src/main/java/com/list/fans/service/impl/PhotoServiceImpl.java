package com.list.fans.service.impl;

import com.list.fans.mapper.PhotoMapper;
import com.list.fans.model.Photo;
import com.list.fans.model.User;
import com.list.fans.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {
    @Autowired
    PhotoMapper photoMapper;

    @Override
    public List<Photo> selectPhoto(String userid) {
        return photoMapper.selectPhoto(userid);
    }

    /**
     * 插入图片
     * @param photo
     */
    @Override
    public void insertPhoto(Photo photo){
        User user=new User();
        photo.setPhotoUserId(user.getUserId());
      photoMapper.insertPhoto(photo);
    }
}
