package com.list.fans.service;

import com.list.fans.model.Comment;

import java.util.List;

public interface CommentService {
    /**
     * 评论
     */
   void insertComment(Comment comment);

    /**
     * 删除评论
     */
   void deleteComment(String commentId);

    /**
     * 根据用户id查询所有评论
     * @param commentUserId
     * @return
     */
    public List<Comment> selectComment(String commentUserId);

    /**
     * 根据帖子id查询本帖子的所有评论
     * @param postid
     * @return
     */
    public List<Comment> selectcommentbypostid(String  postid);
}
