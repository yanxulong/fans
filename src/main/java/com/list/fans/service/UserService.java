package com.list.fans.service;

import com.list.fans.model.User;
import org.apache.ibatis.annotations.Param;

public interface UserService {
    /**
     * 用户登录
     * @param account  账号（手机号/邮箱）
     * @param userPassword  密码
     * @return
     */
    User userLogin(String account,String userPassword);

    /**
     * 修改密码
     * @param userId
     * @param userPassword
     * @return
     */
    User updateUserPassword( String userId,String userPassword);

    /**
     * 查询用户名是否重复
     * @param userName
     * @return
     */
    User selectUserNameIsOk(String userName);
    /**
     * 查询用户电话是否重复
     * @param userTelephone
     * @return
     */
    User selectUserTelephoneIsOk(String userTelephone);
    /**
     * 查询用户邮箱是否重复
     * @param userEmail
     * @return
     */
    User selectUserEmailIsOk(String userEmail);
    /**
     * 注册用户
     * @param user
     * @return
     */
    int insertUser(User user);

    /**
     * 根据用户id查询用户信息
     * @param userid
     * @return
     */

    public User selectUserMessage(String userid);

    /**
     * 根据用户id修改头像@xyb
     * @param photo
     * @param userid
     */
    public void updateUserPhoto(String photo,String userid);

    /**
     * 根据用户id修改用户密码@xyb
     * @param password
     * @param userid
     */
    //public  void updateUserPassword(String password,String userid);

    /**
     * 根据用户id修改用户信息@xyb
     * @param user
     */
    public  void updateUserMessage(User user);

    /**
     * 根据用户id查询用户的粉丝数量
     * @param userid
     * @return
     */
    public  Integer selectfans(String userid);

    /**
     * 用户登录
     * @param userTelephone  手机号
     * @return
     */
    User userLoginByCode(@Param("userTelephone") String userTelephone);
}
